

export function selectAllPokemon(state) {
  const ids = Object.keys(state.pokemon);
  const pokemon = [];

  ids.forEach((id) => {
    pokemon.push(state.pokemon[id]);
  });

  return pokemon;
}

export function selectPokemonItem(state, itemId) {
  // debugger
  let theItem = {};
  
  state.pokemonDetail.items.forEach((item) => {
    if (item.id === parseInt(itemId)) {
      theItem = item;
    }
  });
  return theItem;
}
