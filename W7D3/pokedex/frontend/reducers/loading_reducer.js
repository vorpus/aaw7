import { REQUEST_POKEMON, RECEIVE_ALL_POKEMON, RECEIVE_ONE_POKEMON, RECEIVE_POKEMON_ERRORS } from "../actions/pokemon_actions";

export const loadingReducer = (state = false, action) => {
  switch(action.type) {
    case RECEIVE_ONE_POKEMON:
    case RECEIVE_ALL_POKEMON:
    case RECEIVE_POKEMON_ERRORS:
      return false;
    case REQUEST_POKEMON:
      return true;
    default:
      return state;
  }
};
