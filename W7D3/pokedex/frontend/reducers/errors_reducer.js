import { RECEIVE_POKEMON_ERRORS, RECEIVE_ONE_POKEMON, RECEIVE_ALL_POKEMON } from '../actions/pokemon_actions';

export const errorsReducer = (state = [], action) => {
  switch(action.type) {
    case RECEIVE_ONE_POKEMON:
    case RECEIVE_ALL_POKEMON:
      return [];
    case RECEIVE_POKEMON_ERRORS:
      return action.errors.responseJSON;
    default:
      return state;
  }
};
