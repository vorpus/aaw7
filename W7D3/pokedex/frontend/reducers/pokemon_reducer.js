import { RECEIVE_ALL_POKEMON, RECEIVE_ONE_POKEMON } from '../actions/pokemon_actions';

export const pokemonReducer = (state = {}, action) => {
  let newState;

  switch (action.type) {
    case RECEIVE_ALL_POKEMON:
      newState = action.pokemon ;
      return newState;
    case RECEIVE_ONE_POKEMON:
      let newPokemon = {
        [action.pokemon.id]: action.pokemon
      };
      return Object.assign({}, state, newPokemon);
    default:
      return state;
  }
};
