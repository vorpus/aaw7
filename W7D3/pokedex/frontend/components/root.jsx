import React from 'react';
import { Provider } from 'react-redux';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import { fetchOnePokemon } from '../actions/pokemon_actions';


import PokemonIndexContainer from './pokemon/pokemon_index_container';
import PokemonDetailContainer from './pokemon_detail/pokemon_detail_container';
import ItemDetailContainer from './item/item_detail_container';
import PokemonFormContainer from './pokemon/pokemon_form_container';

const Root = ({ store }) => {
  const someFunction = (nextState, replace, asyncDone) => {
    store.dispatch(fetchOnePokemon(nextState.params.pokemonId)).then(asyncDone)
  }

  return (
    <Provider store={store}>
      <Router history={hashHistory}>
        <Route path="/" component= { PokemonIndexContainer }>
          <IndexRoute component={PokemonFormContainer}/>
          <Route path="pokemon/:pokemonId" component={ PokemonDetailContainer }
          onEnter={someFunction}>
            <Route path="item/:itemId" component={ ItemDetailContainer } />
          </Route>
        </Route>
      </Router>
    </Provider>
  )

}


export default Root;
