import { connect } from 'react-redux';

import { fetchOnePokemon } from '../../actions/pokemon_actions';
import PokemonDetail from './pokemon_detail';

const mapStateToProps = (state) => {
  return {pokemonDetail: state.pokemonDetail};
};

const mapDispatchToProps = (dispatch) => ({
  fetchOnePokemon: (pokeId) => (dispatch(fetchOnePokemon(pokeId))),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PokemonDetail);
