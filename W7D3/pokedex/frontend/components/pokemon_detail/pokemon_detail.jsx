import React from 'react';
import { Link } from 'react-router';

class PokemonDetail extends React.Component {

  componentDidMount() {
    // this.props.fetchOnePokemon(this.props.params.pokemonId);
  }

  componentWillReceiveProps(nextProps, nextState) {
    // if (nextProps.params.pokemonId !== this.props.params.pokemonId) {
    //   this.props.fetchOnePokemon(nextProps.params.pokemonId);
    // }
  }

  render () {
    // debugger
    let items;
    if (this.props.pokemonDetail.items) {
      items = this.props.pokemonDetail.items.map( (item) => {
        return (
          <li key={item.id}>
            <Link to={ `pokemon/${this.props.params.pokemonId}/item/${item.id}` }>
              <img src={item.image_url} />
            </Link>
          </li>
        );
      });
    } else {
      items = "";
    }


    return (
      <section className="pokemon-detail">
        <h1>{this.props.pokemonDetail.name}</h1>
        <img src={this.props.pokemonDetail.image_url}/>
        <h2>Type: {this.props.pokemonDetail.poke_type}</h2>
        <h2>Attack: {this.props.pokemonDetail.attack}</h2>
        <h2>Defense: {this.props.pokemonDetail.defense}</h2>
        <h2>Moves: {this.props.pokemonDetail.moves.join(", ")}</h2>
        <h1>Items:</h1><br/>
        <ul className="item-list">{items}</ul>
        {this.props.children}
      </section>
    );
  }

}

export default PokemonDetail;
