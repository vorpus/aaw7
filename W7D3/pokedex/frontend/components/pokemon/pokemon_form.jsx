import React from 'react';
import { withRouter } from 'react-router';

class PokemonForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = { name: "", image_url: "", poke_type: "fire", attack: "", defense: "", move1: "", move2: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  handleChange (e) {
    e.preventDefault();
    const name = e.currentTarget.name;

    this.setState({
      [name]: e.currentTarget.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const newPokemon = this.makePokemonObject();
    this.props.createPokemon(newPokemon).then((pokemon) => {
      this.props.router.push(`pokemon/${pokemon.id}`);
    });
    this.setState({ name: "", image_url: "", poke_type: "fire", attack: "", defense: "", move1: "", move2: "" });

  }

  makePokemonObject () {
    return {
      name: this.state.name,
      poke_type: this.state.poke_type,
      attack: parseInt(this.state.attack),
      defense: parseInt(this.state.defense),
      moves: [this.state.move1, this.state.move2],
      image_url: this.state.image_url,
    };
  }

  render() {
    const TYPES = [
      "fire",
      "electric",
      "normal",
      "ghost",
      "psychic",
      "water",
      "bug",
      "dragon",
      "grass",
      "fighting",
      "ice",
      "flying",
      "poison",
      "ground",
      "rock",
      "steel"
    ];

    const dropDown = TYPES.map((type) => (
      <option key={type} value={type}>{type}</option>
    ));

    const errors = this.props.errors.map((error, idx) => {
      return (
        <li key={idx}>
          {error}
        </li>
      );
    });

    return (
      <form className="poke-form" onSubmit={this.handleSubmit}>
        <h2>CREATE A POKEMON!</h2>
        <ul className="error-messages">{errors}</ul>
        <label>Name:
          <input name="name" type="text" value={this.state.name} onChange={this.handleChange} />
        </label><br/>
        <label>Type:
          <select name="poke_type" onChange={this.handleChange} >
            {dropDown}
          </select>
        </label><br/>
        <label>Attack:
          <input name="attack" type="text" value={this.state.attack} onChange={this.handleChange} />
        </label><br/>
        <label>Defense:
          <input name="defense" type="text" value={this.state.defense} onChange={this.handleChange} />
        </label><br/>
        <label>Move1:
          <input name="move1" type="text" value={this.state.move1} onChange={this.handleChange} />
        </label><br/>
        <label>Move2:
          <input name="move2" type="text" value={this.state.move2} onChange={this.handleChange} />
        </label><br/>
        <label>Image_url:
          <input name="image_url" type="text" value={this.state.image_url} onChange={this.handleChange} />
        </label><br/>
        <input type="submit" value="Create pokemon" />
      </form>
    );
  }
}



export default withRouter(PokemonForm);
