import React from 'react';

import PokemonIndexItem from './pokemon_index_item';

class PokemonIndex extends React.Component {

  componentDidMount() {
    this.props.fetchAllPokemon();
  }

  render() {
    const pokemon = this.props.pokemon.map((poke) => {
      return (
        <PokemonIndexItem key={poke.id} poke={poke} />
      );
    });

    const spinner = this.props.loading ? (<div className="loader">Loading...</div>) : "";

    const content = (
        <div className="pokedex">
          <ul className="pokemon-index">{pokemon}</ul>
          {this.props.children}
        </div>
      );


    let mainContent = spinner === "" ? content : spinner;

    return (
      <section className="main-content">
        {mainContent}
      </section>
    );
  }

}

export default PokemonIndex;
