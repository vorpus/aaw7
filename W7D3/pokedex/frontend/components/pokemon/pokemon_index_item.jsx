import React from 'react';
import { Link } from 'react-router';

const PokemonIndexItem = (props) => {

  const handleClick = url => e => props.router.push(url);

  return(

    <li>
      <Link to={`/pokemon/${props.poke.id}` }>
        {props.poke.name}<br/><img src={props.poke.image_url}/>
      </Link>
    </li>

  );
};

export default PokemonIndexItem;
