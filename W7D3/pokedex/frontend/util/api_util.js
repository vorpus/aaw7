export function fetchAllPokemon() {
  return $.ajax({
    method: "GET",
    url: "/api/pokemon",
  });
}

export function fetchOnePokemon(id) {
  return $.ajax({
    method: "GET",
    url: `/api/pokemon/${id}`
  });
}

export function createPokemon(pokemon) {
  return $.ajax({
    method: 'POST',
    url: `/api/pokemon`,
    data: { pokemon }
  });
}
