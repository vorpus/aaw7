import React from 'react';
import ReactDOM from 'react-dom';

import Root from './components/root';

import { receiveOnePokemon } from './actions/pokemon_actions';
// import { fetchAllPokemon } from './actions/pokemon_actions';
import { configureStore } from './store/store';
// import { selectAllPokemon } from './reducers/selectors';
import { createPokemon } from './actions/pokemon_actions';
window.createPokemon = createPokemon;
window.receiveOnePokemon = receiveOnePokemon;
// window.fetchAllPokemon = fetchAllPokemon;
// window.selectAllPokemon = selectAllPokemon;


document.addEventListener("DOMContentLoaded", () => {
  const root = document.getElementById('root');
  const store = configureStore();
  window.store = store;
  ReactDOM.render(<Root store={store} />, root);
});
