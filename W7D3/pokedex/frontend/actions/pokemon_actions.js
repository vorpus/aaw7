import * as APIUtil from '../util/api_util';

export const RECEIVE_ALL_POKEMON = "RECEIVE_ALL_POKEMON";
export const RECEIVE_ONE_POKEMON = "RECEIVE_ONE_POKEMON";
export const RECEIVE_POKEMON_ERRORS = "RECEIVE_POKEMON_ERRORS";
export const REQUEST_POKEMON = "REQUEST_POKEMON";

export const receiveAllPokemon = (pokemon) => ({
  type: RECEIVE_ALL_POKEMON,
  pokemon,
});

export const receiveOnePokemon = (pokemon) => ({
  type: RECEIVE_ONE_POKEMON,
  pokemon,
});

export const receivePokemonErrors = (errors) => {
  return {
    type: RECEIVE_POKEMON_ERRORS,
    errors,
  };
};

export const requestPokemon= () => {
  return {
    type: REQUEST_POKEMON,
  };
};

export function fetchAllPokemon() {
  return (dispatch) => {
    dispatch(requestPokemon());
    return APIUtil.fetchAllPokemon().then(pokemon => dispatch(receiveAllPokemon(pokemon)));
  };
}

export function fetchOnePokemon(pokeId) {
  return dispatch => {
    dispatch(requestPokemon());
    return APIUtil.fetchOnePokemon(pokeId).then(poke => dispatch(receiveOnePokemon(poke)));
  };
}

export function createPokemon(pokemon) {
  return dispatch => {
    dispatch(requestPokemon());
    return APIUtil.createPokemon(pokemon)
    .then(poke => {
      dispatch(receiveOnePokemon(poke));
      return poke;
    },
      err => {
        dispatch(receivePokemonErrors(err));
      });
  };
}
