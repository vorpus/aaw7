class Api::PokemonController < ApplicationController
  def index
    @pokemon = Pokemon.all
  end

  def show
    @pokemon = Pokemon.find(params[:id])
  end

  def create
    @pokemon = Pokemon.new(pokemon_params)
    if @pokemon.save
      render :show
    else
      render json: @pokemon.errors.full_messages, status: 404
    end
  end

  private

  def pokemon_params
    params.require(:pokemon).permit(
      :name, :attack, :defense, :image_url, :poke_type, moves: []
    )
  end
end

# pokemon = {name: "ha", attack: 100, defense: 100, image_url: "hjkdfsjkhds", moves:["1", "2"], poke_type: "bug"}
