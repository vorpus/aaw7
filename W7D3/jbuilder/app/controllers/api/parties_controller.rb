class Api::PartiesController < ApplicationController
  def index
    @parties = Party.all.includes(guests: [:gifts])
    render :index
  end

  def show
    @party = Party.where(id: params[:id]).includes(guests: [:gifts]).first
    render :show
  end
end
