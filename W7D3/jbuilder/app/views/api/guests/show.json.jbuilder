json.partial! 'api/guests/guest', guest: @guest

# json.extract! @guest, :gifts
json.gifts do
  @guest.gifts.each do |gift|
    json.title gift.title
    json.description gift.description
  end
end
