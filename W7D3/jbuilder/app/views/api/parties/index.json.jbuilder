json.array! @parties do |party|
  json.guests party.guests
  json.name party.name
end
