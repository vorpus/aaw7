# == Schema Information
#
# Table name: benches
#
#  id          :integer          not null, primary key
#  description :string           not null
#  lat         :float            not null
#  lng         :float            not null
#

class Bench < ActiveRecord::Base
  validates :description, :lat, :lng, presence: true

  def self.in_bounds(bounds)
    # {
    #   "northEast"=> {"lat"=>"37.80971", "lng"=>"-122.39208"},
    #   "southWest"=> {"lat"=>"37.74187", "lng"=>"-122.47791"}
    # }

    # bounds = {"northEast" => {"lat"=>40.7535, "lng"=>-73.99}, "southWest" => {"lat"=>40.7540, "lng"=>-73.98}}
    # Bench.all.where("lat BETWEEN 40.7535 AND 40.7540").where("lng BETWEEN -73.99 AND -73.98")
    Bench.all
      .where("lat BETWEEN #{bounds["northEast"]["lat"]} AND #{bounds["southWest"]["lat"]}")
      .where("lng BETWEEN #{bounds["northEast"]["lng"]} AND #{bounds["southWest"]["lng"]}")
  end
end
