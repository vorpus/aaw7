export const UPDATE_BOUNDS = "UPDATE_BOUNDS";

import {fetchBenches} from './bench_actions';

export const updateBounds = (bounds) => {
  return {
    type: UPDATE_BOUNDS,
    bounds
  };
};

// export function updateBounds(bounds) {
//   return (dispatch, getState) => {
//     debugger
//     dispatch(receiveBounds(bounds));
//     return fetchBenches(getState().filters)(dispatch);
//   };
// }

// export function updateFilter(filter, value) {
//   return (dispatch, getState) => {
//     dispatch(changeFilter(filter, value));
//     return fetchBenches(getState().filters)(dispatch);
//     // delicious curry!
//   };
// }
