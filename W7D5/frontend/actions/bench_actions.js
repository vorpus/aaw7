export const RECEIVE_BENCHES = "RECEIVE_BENCHES";

import * as APIUtil from '../util/api_util';

export const receiveBenches = (benches) => {
  return {
    type: RECEIVE_BENCHES,
    benches
  };
};

export const fetchBenches = (bounds) => {
  // debugger
  bounds = {"northEast": {"lat":40, "lng":-74}, "southWest": {"lat":41, "lng":-73}};
  return (dispatch) => {
    return APIUtil.getBenches(bounds).then(
      (success) => dispatch(receiveBenches(success)),
      (err) => dispatch(receiveErrors(err))
    );
  };
};
