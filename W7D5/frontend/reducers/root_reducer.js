import { combineReducers } from 'redux';
import SessionReducer from './session_reducer';
import errorsReducer from './errors_reducer';
import benchesReducer from './benches_reducer';
import FilterReducer from './filter_reducer';

const rootReducer = combineReducers({
  session: SessionReducer,
  errors: errorsReducer,
  benches: benchesReducer,
  filters: FilterReducer
});

export default rootReducer;
