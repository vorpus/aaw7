import {UPDATE_BOUNDS} from "../actions/filter_actions";

const initialBounds = {"northEast": {"lat":40.7535, "lng":-73.99}, "southWest": {"lat":40.7540, "lng":-73.98}};

const FilterReducer = (state = initialBounds, action) => {
  Object.freeze(state);
  switch(action.type) {
      case UPDATE_BOUNDS:
        return action.bounds;
      default:
        return state;
  }
};

export default FilterReducer;
