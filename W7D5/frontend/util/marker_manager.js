export default class MarkerManager {
  constructor(map) {
    this.map = map;
    this.markers = [];
  }

  updateMarkers() {
    console.log('time to update');

    let newBenches = this._benchesToAdd();
    let expiredBenches = this._benchesToRemove();
    newBenches.forEach((bench) => {
      this._createMarkerFromBench(bench);
    });
    expiredBenches.forEach((bench) => {
      console.log('removing a marker');
      this._removeMarker(bench);
    });
  }

  _benchesToAdd() {
    let allBenches = store.getState().benches;
    let benchToAdd = [];

    Object.keys(allBenches).forEach((benchIdx) => {
      if (!this.markers[benchIdx]) {
        benchToAdd.push(allBenches[benchIdx]);
      }
    });

    return benchToAdd;
  }

  _benchesToRemove() {
    let curBounds = store.getState().filters;

    let benchToRemove = [];
    this.markers.forEach((marker) => {
      marker.position.lat();
      marker.position.lng();
      if (marker.position.lat() > curBounds.southWest.lat ||
          marker.position.lat() < curBounds.northEast.lat) {
        benchToRemove.push(marker);
      } else if (marker.position.lng() < curBounds.southWest.lng ||
                  marker.position.lng() > curBounds.northEast.lng) {
        benchToRemove.push(marker);
      }
    });
    return benchToRemove;
  }

  _removeMarker(marker) {
    marker.setMap(null);
  }

  _createMarkerFromBench(bench) {
    this.markers.push(new google.maps.Marker({
      position: {lat: bench.lat, lng: bench.lng},
      map: this.map,
      title: bench.description
    }));
  }
}
