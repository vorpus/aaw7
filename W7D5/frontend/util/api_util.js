export function createUser (user) {
  return $.ajax({
    method: 'POST',
    url: '/api/users',
    data: { user: user }
  });
}

export function loginUser (user) {
  return $.ajax({
    method: 'POST',
    url: '/api/session',
    data: { user: user }
  });
}

export function logoutUser () {
  return $.ajax({
    method: 'DELETE',
    url: `/api/session/`
  });
}

export function getBenches (filters) {
  return $.ajax({
    method: 'GET',
    url: '/api/benches',
    data: { bounds: filters }
  });
}

export function createBench(bench) {
  return $.ajax({
    method: 'POST',
    url: '/api/benches',
    data: {bench}
  });
}
