import { connect } from 'react-redux';

import { fetchBenches } from '../actions/bench_actions';
import {selectAllBenches} from '../reducers/selectors';
import BenchIndex from './bench_index';

const mapStateToProps = (state) => {
  // debugger
  return {
    benches: state.benches
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchBenches: () => dispatch(fetchBenches())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BenchIndex);
