import React from 'react';
import MarkerManager from '../util/marker_manager';

class BenchMap extends React.Component {

  componentDidMount () {
    const mapOptions = {
          center: { lat: 40.7283, lng: -73.9858 },
          zoom: 13
        };

        // wrap the mapDOMNode in a Google Map
    this.map = new google.maps.Map(this.mapNode, mapOptions);
    this.MarkerManager = new MarkerManager(this.map);

    this.map.addListener("idle", () => {
      const mapBounds = this.map.getBounds();
      let bounds = {"northEast": {"lat": mapBounds.f.f, "lng": mapBounds.b.f},
                    "southWest": {"lat": mapBounds.f.b, "lng": mapBounds.b.b}};
      this.props.updateBounds(bounds);
      console.log(`updated bounds to ${mapBounds.f.f}, ${mapBounds.b.f}`);
      this.MarkerManager.updateMarkers();
    });


  }

  componentDidUpdate () {
    this.MarkerManager.updateMarkers(this.props.benches);
  }

  render() {
    return (
      <div id="map-container" ref={map => this.mapNode = map}>

      </div>
    );
  }
}

export default BenchMap;
