import { connect } from 'react-redux';

import { fetchBenches } from '../actions/bench_actions';
import { updateBounds } from '../actions/filter_actions';
import {selectAllBenches} from '../reducers/selectors';
import Search from './search';

const mapStateToProps = (state) => {
  // debugger
  return {
    benches: state.benches
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchBenches: () => dispatch(fetchBenches()),
    updateBounds: (bounds) => dispatch(updateBounds(bounds))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
