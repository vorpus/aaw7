import React from 'react';

class BenchIndex extends React.Component {

  componentDidMount() {
    this.props.fetchBenches();
  }

  render() {

    let benchItems = Object.values(this.props.benches).map((e, idx)=>{
      return <li key={idx}>{e.description}, {e.lat}, {e.lng}</li>;
    });


    return(
      <ul>{benchItems}</ul>
    );
  }
}

export default BenchIndex;
