import React from 'react';

import GreetingContainer from './greeting_container';

const App = ({ children }) => (
  <div>
    <h1>bench bnb</h1>
    <GreetingContainer />
    {children}
  </div>
);

export default App;
