import React from 'react';
import { Provider } from 'react-redux';

import App from './app';
import SessionFormContainer from './session_form_container';
import SearchContainer from './search_container';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

class Root extends React.Component {
  constructor(props) {
    super(props);
  }

  _redirectIfLoggedIn (_, replace) {
    if (store.getState().session.currentUser) {
      replace('/');
    }
  }

  render() {
    return (
      <Provider store={ this.props.store }>
        <Router history={ hashHistory }>
          <Route path="/" component={ App }>
            <IndexRoute component={SearchContainer} />
            <Route path="/login" component={SessionFormContainer} onEnter={this._redirectIfLoggedIn} />
            <Route path="/signup" component={SessionFormContainer} onEnter={this._redirectIfLoggedIn} />
          </Route>
        </Router>
      </Provider>
    );
  }
}

export default Root;
