# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Bench.create(description:"washington sq park", lat:40.730836, lng:-73.997357)
Bench.create(description:"columbus park", lat:40.7156450, lng:-73.999968)
Bench.create(description:"the high line", lat:40.747999, lng:-74.004789)
Bench.create(description:"bryant park", lat:40.753513, lng:-73.983027)
Bench.create(description:"frisbee hill", lat:40.771773, lng:-73.973760)
Bench.create(description:"kennedy bridge", lat:40.779777, lng:-73.926728)
Bench.create(description:"brooklyn bridge", lat:40.705423, lng:-73.996353)
Bench.create(description:"nyse", lat:40.706004, lng:-74.011542)
