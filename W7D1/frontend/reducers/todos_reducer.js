import { RECEIVE_TODOS, RECEIVE_TODO, REMOVE_TODO } from '../actions/todo_actions';
import merge from 'lodash/merge';

const initialState = {
  "1": {
    id: 1,
    title: "wash car",
    body: "with soap",
    done: false
  },
  "2": {
    id: 2,
    title: "wash dog",
    body: "with shampoo",
    done: true
  },
};

const todosReducer = (state = initialState, action) => {

  Object.freeze(state);

  switch(action.type) {
    case RECEIVE_TODOS:
    //{}, oldState, newState Obj
    const newState = {};

    for (let i = 0; i < action.todos.length; i++){
      newState[action.todos[i].id] = action.todos[i];
    }

      return newState;

    case RECEIVE_TODO:
      return merge({}, state, {
        [action.todo.id]: action.todo
      });

    case REMOVE_TODO:
      let removeTodo = merge({}, state);
      delete removeTodo[action.todo.id];
      return removeTodo;

    case "CLEAR":
      return initialState;

    default:
      return state;
  }
};

export default todosReducer;
