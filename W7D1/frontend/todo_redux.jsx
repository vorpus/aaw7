import React from 'react';
import ReactDOM from 'react-dom';

import store from './store/store';
import allTodos from './reducers/selectors';
import {receiveTodos, receiveTodo, removeTodo } from './actions/todo_actions';

import Root from './components/root';

// debugger

// //TODO: remove this
// window.store = store;
// window.allTodos = allTodos;
// window.receiveTodos = receiveTodos;
// window.receiveTodo = receiveTodo;





document.addEventListener("DOMContentLoaded", () => {
  let content = document.getElementById('content');
  ReactDOM.render(<Root store={ store } />, content);
});
