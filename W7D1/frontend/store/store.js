import { createStore } from 'redux';
//{} unless exporting default
import rootReducers from '../reducers/root_reducer';

function configureStore () {
  return createStore(rootReducers);
}

const store = configureStore();
export default store;
