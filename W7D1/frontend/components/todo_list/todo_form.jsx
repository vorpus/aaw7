import React from 'react';

import { uniqueId } from '../../util/util';

class TodoForm extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      newTodo: ''
    };

    this.handleTextChange = this.handleTextChange.bind(this);
    this.submitTodo = this.submitTodo.bind(this);
  }

  handleTextChange(e) {
    this.setState({ newTodo: e.currentTarget.value });
  }

  submitTodo (e) {
    e.preventDefault();

    let newObj = {
      id: uniqueId(),
      title: this.state.newTodo
    };
    this.props.receiveTodo(newObj);

    this.setState({ newTodo: '' });
  }


  render() {
  return (
    <form onSubmit={this.submitTodo}>
      <input type='text' value={this.state.newTodo} onChange={this.handleTextChange} />
      <br />
      <input type='submit' value='Make a New Todo!' />
    </form>
    );
  }
}

export default TodoForm;
