import { connect } from 'react-redux';
import TodoList from './todo_list';

import allTodos from '../../reducers/selectors';
import {receiveTodos, receiveTodo, removeTodo } from '../../actions/todo_actions';

const mapStateToProps = (state) => ({
  todos: allTodos(state)
});

const mapDispatchToProps = (dispatch) => ({
  receiveTodo: (newTodo) => dispatch(receiveTodo(newTodo)),
  removeTodo: (todoToRemove) => dispatch(removeTodo(todoToRemove)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);
