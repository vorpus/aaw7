# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Todo.create!(title: "buy some nikes", body: "sell them", done: false)
Todo.create!(title: "finish rails app", body: "rock it", done: false)
Todo.create!(title: "buy good lunch", body: "eat it", done: false)
Todo.create!(title: "take a nap", body: "1pm-8pm", done: true)
