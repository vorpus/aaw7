
const allTodos = (state) => {
  return Object.keys(state.todos).map((key, idx) => {
    return state.todos[key];
  });
};

export default allTodos;
