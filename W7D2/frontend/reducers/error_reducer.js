import { RECEIVE_ERRORS, CLEAR_ERRORS } from "../actions/error_actions";

const initialState = [];

const errorReducer = (state = initialState, action) => {
  // debugger
  switch(action.type) {
    case RECEIVE_ERRORS:
      return action.errors;
    case CLEAR_ERRORS:
      return initialState;
    default:
      return initialState;
  }
};

export default errorReducer;
