import { combineReducers } from 'redux';

import todos from './todos_reducer';
import errors from './error_reducer';
//exporting a function that createStore will use as a callback
export default combineReducers({
  todos, errors
});
