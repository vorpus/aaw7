import * as APIUtil from '../util/todo_api_util';

import { receiveErrors, clearErrors } from './error_actions';

export const RECEIVE_TODOS = "RECEIVE_TODOS";
export const RECEIVE_TODO = "RECEIVE_TODO";
export const REMOVE_TODO = "REMOVE_TODO";
export const UPDATE_TODO = "UPDATE_TODO";
export const FETCH_TODOS = "FETCH_TODOS";
export const CREATE_TODO = "CREATE_TODO";



export const receiveTodos = (todos) => {
  return {
    type: RECEIVE_TODOS,
    todos,
  };
};

export const receiveTodo = (todo) => {
  return {
    type: RECEIVE_TODO,
    todo,
  };
};

export const removeTodo = (todo) => {
  return {
    type: REMOVE_TODO,
    todo,
  };
};

// export const updateTodo = (todo) => {
//   return {
//     type: UPDATE_TODO,
//     todo,
//   };
// };

export const fetchTodos = () => {
  // debugger
  return (dispatch) => {
    // debugger
    //come back to me
    dispatch({ type: FETCH_TODOS });
    return APIUtil.fetchAllTodos().then(todos => {
      // debugger
      dispatch(receiveTodos(todos));
    });
  };
};

// export const createTodo = (todo) => {
//   // debugger
//   return (dispatch) => {
//     // debugger
//     //come back to me
//     dispatch({ type: CREATE_TODO, todo: todo });
//     return APIUtil.createTodo(todo);
//   };
// };

export function createTodo(todo) {
  return (dispatch) => {
    return APIUtil.createTodo(todo)
      .then(todo => dispatch(receiveTodo(todo)),
            err => dispatch(receiveErrors(err.responseJSON)));
  };
}

export function updateTodo(todo) {
  return (dispatch) => {
    return APIUtil.updateTodo(todo)
      .then(todo => dispatch(receiveTodo(todo)),
            err => dispatch(receiveErrors(err.responseJSON)));
  };
}

export function deleteTodo(todo) {
  return (dispatch) => {
    // debugger
    return APIUtil.deleteTodo(todo)
      .then(todo => dispatch(removeTodo(todo)),
            err => dispatch(receiveErrors(err.responseJSON)));
  };
}
