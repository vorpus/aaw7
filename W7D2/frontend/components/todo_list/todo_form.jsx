import React from 'react';

import { uniqueId } from '../../util/util';

class TodoForm extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      newTodo: '',
      newTodoBody: '',
      done: false
    };

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleBodyChange = this.handleBodyChange.bind(this);
    this.submitTodo = this.submitTodo.bind(this);
    this.errorsThing = this.errorsThing.bind(this);
  }

  handleTextChange(e) {
    this.setState({ newTodo: e.currentTarget.value });
  }

  handleBodyChange(e) {
    this.setState({ newTodoBody: e.currentTarget.value });
  }

  submitTodo (e) {
    e.preventDefault();

    let newObj = {
      id: uniqueId(),
      title: this.state.newTodo,
      body: this.state.newTodoBody,
      done: false
    };
    this.props.createTodo(newObj).then(
      () => this.setState({ newTodo: "", newTodoBody: "" })
    );
  }

  errorsThing () {

    return this.props.errors.map((err, idx) => {
      // debugger
      return(
        <li key={idx}>
          {err}
        </li>
      );
    });
  }


  render() {
  return (
    <div>
      <form className="todo-form" onSubmit={this.submitTodo}>
      <label>Title:</label>
        <input type='text' value={this.state.newTodo} onChange={this.handleTextChange} />
        <br />
        <label>Body:</label>
        <input type='text' value={this.state.newTodoBody} onChange={this.handleBodyChange}></input>
        <br />
        <input type='submit' value='Make a New Todo!' />
      </form>
      <ul>
        {this.errorsThing()}
      </ul>
    </div>

    );
  }
}

export default TodoForm;
