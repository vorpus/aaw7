import { connect } from 'react-redux';
import TodoList from './todo_list';

import allTodos from '../../reducers/selectors';
import { receiveTodos,
  receiveTodo,
  removeTodo,
  updateTodo,
  fetchTodos,
  createTodo,
  deleteTodo
} from '../../actions/todo_actions';

import {receiveErrors} from '../../actions/error_actions';

const mapStateToProps = (state) => {
  return {
    todos: allTodos(state),
    errors: state.errors,
  };
};

const mapDispatchToProps = (dispatch) => ({
  createTodo: (newTodo) => dispatch(createTodo(newTodo)),
  removeTodo: (todoToRemove) => dispatch(removeTodo(todoToRemove)),
  updateTodo: (todoToUpdate) => dispatch(updateTodo(todoToUpdate)),
  fetchTodos: () => dispatch(fetchTodos()),
  deleteTodo: (todoToDelete) => dispatch(deleteTodo(todoToDelete))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);
