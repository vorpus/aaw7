import React from 'react';






const TodoListItem = (props) => {

  function toggleComplete(todo) {
    todo.done = !todo.done;
    return todo;
  }

  return (
    <li className="list-item group">
      <strong>{props.todo.title}</strong>
      <br />
      {props.todo.body}
      <button onClick={() => {props.deleteTodo(props.todo);} }>delete</button>
      <button onClick={() => {props.updateTodo(toggleComplete(props.todo));} }>{ props.todo.done ? 'done' : 'undo' }</button>
    </li>
  );
};

export default TodoListItem;
