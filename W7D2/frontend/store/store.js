import { createStore, applyMiddleware } from 'redux';
import thunk from '../middleware/thunk';
//{} unless exporting default
import rootReducers from '../reducers/root_reducer';

function configureStore () {
  return createStore(rootReducers, applyMiddleware(thunk));
}

const store = configureStore();
export default store;
