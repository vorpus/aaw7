export function fetchAllTodos(){
  return $.ajax({
    method: "GET",
    url: "/api/todos"
  });
}

export function createTodo(todo){
  return $.ajax({
    method: "POST",
    url: "/api/todos",
    data: {todo},
  });
}

export function updateTodo(todo) {
  // debugger
  return $.ajax({
    method: "PATCH",
    url: `/api/todos/${todo.id}`,
    data: {todo}
  });
}

export function deleteTodo(todo) {
  return $.ajax({
    method: "DELETE",
    url: `/api/todos/${todo.id}`,
    data: {todo}
  });
}
